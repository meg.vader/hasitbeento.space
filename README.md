# Has It Been To Space
Have you ever wondered whether something has been to space? Whether it's a dog, monkey, wrist watch, golf ball, or whatever you may be thinking of? Well this service hopes to answer that question for you. And who knows, maybe you'll end up being the first to send something to space!

# Data
All data is crowd-sourced. Anyone can either submit an issue on this Gitlab project or submit a merge request to add additional data and information to the database. All objects and things must be verifiable as having been sent into space (see 'Definition of Space'). At minimum, all that is required is a name for the thing with a link to a verifiable source. Optional information can include an image, or link to an image, as well as a brief description (less than 240 characters) of the thing.

# Definition of "Space"
While there is contention on what defines the edge of space, HasItBeenTo.Space will be using the generally accepted definition of the Kármán line for all items in the database. That means an object or thing must have passed the 100 kilometer mark (62 miles, 330,000 feet) above mean sea level for it to be considered as "in space". Sorry, Taco Bell, sending a taco up on a weather balloon doesn't count! That's the only requirement to be listed in this database, otherwise it doesn't matter when, what country, or what vehicle send a thing to space for it to be included.

# Rules for Inclusion
The only three rules for inclusion into the HasItBeenTo.Space database are:
1. It must have crossed the Kármán line (see "Definition of Space" above)
2. It must be verifiable through official sources
3. No people, companies, or organizations
